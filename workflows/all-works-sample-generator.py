from airflow.decorators import dag, task

from datetime import date
from datetime import datetime
from datetime import timedelta

import boto3
import json
import base64


def get_secret(secret_name):
    region_name = "us-east-1"

    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )

    get_secret_value_response = client.get_secret_value(SecretId=secret_name)
    if 'SecretString' in get_secret_value_response:
        return get_secret_value_response['SecretString']
    else:
        return base64.b64decode(get_secret_value_response['SecretBinary'])


API_URL = 'https://api.crossref.org'
MAILTO = get_secret('sampling-framework-mailto')

CODE_BUCKET = get_secret('bucket-name-emr-code')
BOOTSTRAP_SCRIPT = f's3://{CODE_BUCKET}/sampling-framework/bootstrap.sh'
SAMPLES_SCRIPT = f's3://{CODE_BUCKET}/sampling-framework/generate-samples.py'

TODAY = str(date.today())
SAMPLES_PREFIX = f'all-works/{TODAY}'
SAMPLES_BUCKET = get_secret('bucket-name-samples-data')
SAMPLE_CONTEXTS_FILE = 'all-works/context.jsonl'
SAMPLE_CONTEXTS_PATH = f's3://{SAMPLES_BUCKET}/{SAMPLE_CONTEXTS_FILE}'
 
DEFAULT_ARGS = {
    'owner': 'dtkaczyk',
    'depends_on_past': False,
    'email': [get_secret('dtkaczyk-mailto')],
    'email_on_failure': True,
    'email_on_retry': True,
}


@dag(
    default_args=DEFAULT_ARGS,
    schedule_interval='0 1 * * 0',
    catchup=False,
    dagrun_timeout=timedelta(hours=2),
    start_date=datetime(2022, 11, 23),
    tags=['sampling']
)
def all_works_sample():

    @task()
    def generate_sample_context():
        context = {'name': 'all works sample',
                   'api-request': 'works',
                   's3-prefix': 'sample',
                   'sample-size': 5000}
        session = boto3.Session()
        s3 = session.resource('s3')
        obj = s3.Object(SAMPLES_BUCKET, SAMPLE_CONTEXTS_FILE)
        obj.put(Body=json.dumps(context))
        return True

    @task()
    def start_spark_app(contexts_saved: bool):
        client = boto3.client('emr', region_name='us-east-1')
        response = client.run_job_flow(
            Name='all-works-sample',
            ReleaseLabel='emr-5.36.0',
            Applications=[{
                'Name': 'Spark'
            }],
            Instances={
                'MasterInstanceType': 'm5.xlarge',
                'SlaveInstanceType': 'm5.xlarge',
                'InstanceCount': 2,
                'KeepJobFlowAliveWhenNoSteps': False,
                'TerminationProtected': False
            },
            Steps=[{
                'Name': 'all-works-sample',
                'ActionOnFailure': 'TERMINATE_CLUSTER',
                'HadoopJarStep': {
                    'Jar': 'command-runner.jar',
                    'Args': ['spark-submit',
                             '--deploy-mode',
                             'cluster',
                             SAMPLES_SCRIPT,
                             json.dumps({'contexts': SAMPLE_CONTEXTS_PATH,
                                         'samples-bucket': SAMPLES_BUCKET,
                                         'samples-prefix': SAMPLES_PREFIX,
                                         'samples-metadata': 'sample-metadata.json',
                                         'output-jsonl': 1,
                                         'api-url': API_URL,
                                         'mailto': MAILTO})
                    ]
                }
            }],
            BootstrapActions=[{
                'Name': 'Python packages',
                'ScriptBootstrapAction': {
                    'Path': BOOTSTRAP_SCRIPT
                }
            }],
            LogUri=get_secret('s3-url-logs'),
            VisibleToAllUsers=True,
            ServiceRole='EMR_DefaultRole',
            JobFlowRole='EMR_EC2_DefaultRole'
        )
        return response

    @task()
    def wait_for_spark(spark_response: dict):
        client = boto3.client('emr', region_name='us-east-1')
        waiter = client.get_waiter('cluster_terminated')
        waiter.wait(
            ClusterId=spark_response['JobFlowId'],
            WaiterConfig={
                'Delay': 60,
                'MaxAttempts': 120
            }
        )
        return True

    @task()
    def delete_contexts(samples_generated: bool):
        session = boto3.Session()
        s3 = session.resource('s3')
        obj = s3.Object(SAMPLES_BUCKET, SAMPLE_CONTEXTS_FILE)
        obj.delete()

    contexts_saved = generate_sample_context()
    spark_cluster = start_spark_app(contexts_saved)
    samples_generated = wait_for_spark(spark_cluster)
    delete_contexts(samples_generated)

workflow = all_works_sample()

