from airflow.decorators import dag, task

from datetime import date
from datetime import datetime
from datetime import timedelta

import boto3
import json
import base64
import re
import requests


def get_secret(secret_name):
    region_name = "us-east-1"

    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )

    get_secret_value_response = client.get_secret_value(SecretId=secret_name)
    if 'SecretString' in get_secret_value_response:
        return get_secret_value_response['SecretString']
    else:
        return base64.b64decode(get_secret_value_response['SecretBinary'])


API_URL = 'https://api.crossref.org'
MAILTO = get_secret('sampling-framework-mailto')

CODE_BUCKET = get_secret('bucket-name-emr-code')
BOOTSTRAP_SCRIPT = f's3://{CODE_BUCKET}/sampling-framework/bootstrap.sh'
SAMPLES_SCRIPT = f's3://{CODE_BUCKET}/sampling-framework/generate-samples.py'

TODAY = str(date.today())
SAMPLES_PREFIX = f'members-works/{TODAY}'
SAMPLES_BUCKET = get_secret('bucket-name-samples-data')
SAMPLES_CONTEXTS_FILE = 'members-works/contexts.jsonl'
SAMPLES_CONTEXTS_PATH = f's3://{SAMPLES_BUCKET}/{SAMPLES_CONTEXTS_FILE}'
INDEX_PREFIX = 'members-works/index'

DEFAULT_ARGS = {
    'owner': 'dtkaczyk',
    'depends_on_past': False,
    'email': [get_secret('dtkaczyk-mailto')],
    'email_on_failure': True,
    'email_on_retry': True,
}


@dag(
    default_args=DEFAULT_ARGS,
    schedule_interval='0 0 * * 0',
    catchup=False,
    dagrun_timeout=timedelta(hours=10),
    start_date=datetime(2022, 11, 23),
    tags=['sampling']
)
def members_works_samples():

    @task()
    def generate_sample_contexts():
        contexts = []
        offset = 0
        while True:
            members = requests.get('https://api.crossref.org/members',
                                   {'rows': 1000, 'offset': offset}).json()['message']['items']
            if not members:
                break
            for m in members:
                contexts.append({'name': f'member-{m["id"]}',
                                 'api-request': f'members/{m["id"]}/works',
                                 's3-prefix': f'samples/member-{m["id"]}',
                                 'sample-size': 1000})
            offset += 1000

        session = boto3.Session()
        s3 = session.resource('s3')
        obj = s3.Object(SAMPLES_BUCKET, SAMPLES_CONTEXTS_FILE)
        obj.put(Body='\n'.join([json.dumps(c) for c in contexts]))
        return True

    @task()
    def start_spark_app(contexts_saved: bool):
        client = boto3.client('emr', region_name='us-east-1')
        response = client.run_job_flow(
            Name='members-works-samples',
            ReleaseLabel='emr-5.36.0',
            Applications=[{
                'Name': 'Spark'
            }],
            Instances={
                'MasterInstanceType': 'm5.xlarge',
                'SlaveInstanceType': 'm5.xlarge',
                'InstanceCount': 17,
                'KeepJobFlowAliveWhenNoSteps': False,
                'TerminationProtected': False
            },
            Steps=[{
                'Name': 'members-works-samples',
                'ActionOnFailure': 'TERMINATE_CLUSTER',
                'HadoopJarStep': {
                    'Jar': 'command-runner.jar',
                    'Args': ['spark-submit',
                             '--deploy-mode',
                             'cluster',
                             SAMPLES_SCRIPT,
                             json.dumps({'contexts': SAMPLES_CONTEXTS_PATH,
                                         'samples-bucket': SAMPLES_BUCKET,
                                         'samples-prefix': SAMPLES_PREFIX,
                                         'output-jsonl': 1,
                                         'api-url': API_URL,
                                         'mailto': MAILTO})
                    ]
                }
            }],
            BootstrapActions=[{
                'Name': 'Python packages',
                'ScriptBootstrapAction': {
                    'Path': BOOTSTRAP_SCRIPT
                }
            }],
            LogUri=get_secret('s3-url-logs'),
            VisibleToAllUsers=True,
            ServiceRole='EMR_DefaultRole',
            JobFlowRole='EMR_EC2_DefaultRole'
        )
        return response

    @task()
    def wait_for_spark(spark_response: dict):
        client = boto3.client('emr', region_name='us-east-1')
        waiter = client.get_waiter('cluster_terminated')
        waiter.wait(
            ClusterId=spark_response['JobFlowId'],
            WaiterConfig={
                'Delay': 60,
                'MaxAttempts': 600
            }
        )
        return True

    @task()
    def update_index(samples_generated: bool):
        s3client = boto3.client('s3')
        paginator = s3client.get_paginator('list_objects_v2')
        session = boto3.Session()
        s3 = session.resource('s3')
        for page in paginator.paginate(Bucket=SAMPLES_BUCKET, Prefix=f'{SAMPLES_PREFIX}/samples/'):
            for obj in page['Contents']:
                filename = re.sub('\..*', '', obj['Key'].split('/')[-1])
                key = f'{INDEX_PREFIX}/{filename}'
                try:
                    data = s3client.get_object(Bucket=SAMPLES_BUCKET, Key=key)['Body'].read().decode('utf-8')
                    data = [l.strip() for l in data.split('\n') if l.strip()]
                except s3client.exceptions.NoSuchKey:
                    data = []
                data.append(obj['Key'])
                index_obj = s3.Object(SAMPLES_BUCKET, key)
                index_obj.put(Body='\n'.join(data))

    @task()
    def delete_contexts(samples_generated: bool):
        session = boto3.Session()
        s3 = session.resource('s3')
        obj = s3.Object(SAMPLES_BUCKET, SAMPLES_CONTEXTS_FILE)
        obj.delete()

    contexts_saved = generate_sample_contexts()
    spark_cluster = start_spark_app(contexts_saved)
    samples_generated = wait_for_spark(spark_cluster)
    delete_contexts(samples_generated)
    update_index(samples_generated)

workflow = members_works_samples()

