# Sampling Framework

Sampling framework can be used to efficiently generate samples of works. It is a Spark application and can be run in AWS EMR.

## How to run

**Step 1**. Prepare a context file that specifies the samples to be created, and upload it to S3. The context file can be prepared manually or by an automated process.

The context file is a JSON-Lines file, where each line is a JSON object describing a single sample to be created. The following fields are supported:

  * `name` - the name of the sample.
  * `api-request` - REST API request that specifies the population from which the sample will be drawn.
  * `query-parameters` (optional) - a dictionary of REST API query parameters that further specifies the population from which the sample will be drawn. For example, including `"api-request": "types/journal/works", "query-parameters": {"filter": [{"has-funder": "true"}, {"has-affiliation": "false"}], "select": ["DOI"]}` in the sample context will result in a sample of works of type `journal` that have funder information, do not contain any affiliations, and only the DOI will be present in the sample data points.
  * `sample-size` (optional) - the size of the requested sample. If not provided, a size of 5 is used.
  * `min-population-size` (optional) - a sample will be drawn only if the population has at least min-population-size elements.
  * `s3-prefix` - S3 prefix where the resulting sample will be stored.

An example context file:

    {"name": "type book", "api-request": "types/book/works", "sample-size": 10, "s3-prefix": "works/type/book"}
    {"name": "journals with funders but without affiliations", "api-request": "types/journal/works", "query-parameters": {"filter": [{"has-funder": "true"}, {"has-affiliation": "false"}], "select": ["DOI"]}, "sample-size": 10, "s3-prefix": "works/type/journal"}
    {"name": "overall works sample", "api-request": "works", "sample-size": 30, "s3-prefix": "works/overall"}
    {"name": "journal articles from member 12", "api-request": "members/12/works", "query-parameters": {"filter": [{"type":"journal-article"}]}, "s3-prefix": "works/member/12"}

**Step 2**. Start an EMR cluster that will run the Spark application. You can use the AWS console, AWS client, or the `run.py` script for that:

    python run.py -c <S3 contexts file path> -b <output bucket name> -p <output prefix> [-n <cluster name>] [-a <REST API URL>] [-l]

The script accepts the following parameters:

  * `-c` - input S3 contexts file path
  * `-b` - output bucket name
  * `-p` - output prefix
  * `-n` - (optional) cluster name, default samples-collection
  * `-a` - (optional) REST API URL, default https://api.crossref.org
  * `-l` - (optional) whether to store the output in JSON lines format (one file per sample), if not provided, the output will be stored in JSON format (one file per data point)

The Spark application will download the samples and save them in S3 in `<output bucket name>/<output prefix>`.
