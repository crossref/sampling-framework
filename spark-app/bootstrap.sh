#!/bin/bash -xe

sudo pip3 install --upgrade pip
sudo /usr/local/bin/pip3 install requests
sudo /usr/local/bin/pip3 install boto3
sudo /usr/local/bin/pip3 install retry
