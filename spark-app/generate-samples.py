import boto3
import json
import random
import requests
import sys

from datetime import date
from pyspark.sql import SparkSession
from pyspark.sql.functions import sum
from pyspark.sql.types import StringType
from retry import retry

CONFIG = json.loads(sys.argv[1])

SAMPLES_CONTEXTS = CONFIG['contexts']

TODAY = str(date.today())
SAMPLES_BUCKET = CONFIG['samples-bucket']
SAMPLES_PREFIX = CONFIG.get('samples-prefix', f'samples-{TODAY}')
OUTPUT_FILE = CONFIG.get('data-point-file', 'data-point.json')
METADATA_FILE = CONFIG.get('samples-metadata', 'samples-metadata.json')

OUTPUT_JSONL = CONFIG.get('output-jsonl', '')
EMPTY_SAMPLES = f"s3://{SAMPLES_BUCKET}/{SAMPLES_PREFIX}/empty-samples-log"
API_URL = CONFIG['api-url']
MAILTO = CONFIG['mailto']


def stringify_array(l):
    return ','.join(l)

def nested_list(l):
    return [x for sublist in l for x in sublist]

def process_filter_params(f):
    k = map(lambda x: x.keys(), f)
    v = map(lambda x: x.values(), f)
    flattened_k = nested_list(k)
    flattened_v = nested_list(v)
    filter_query = map(lambda x: ':'.join(x), zip(flattened_k, flattened_v))
    q = stringify_array(filter_query)
    return q


# expecting query parameters to look like the following:
# {"name": "type journal", "api-request": "types/journal/works", "s3-prefix": "works/type/journal", 'query-parameters': {'filter': [{'has-funder': 'true'}, {'has-affiliation': 'true'}], 'select': ['DOI']}}
# OR
# {"name": "overall works sample", "api-request": "works", "s3-prefix": "works/overall", 'query-parameters': {'filter': [{"type":"journal-article"}]}}
def process_query_parameters(q):
    for k, v in q.items():
        if k == 'filter':
            q['filter'] = process_filter_params(v)
        elif isinstance(v, list):
            q[k] = stringify_array(v)
    return q

@retry(tries=10, delay=1, backoff=2, max_delay=300)
def get_population_info(url, query_params):
    params = {'rows': 1000}
    params.update(query_params)
    population_data = requests.get(url, params).json()['message']
    return population_data['total-results'], population_data['items']

@retry(tries=10, delay=1, backoff=2, max_delay=300)
def get_population(url, query_params):
    params = {'rows': 1000}
    params.update(query_params)
    cursor = '*'
    population_data = []
    while True:
        params['cursor'] = cursor
        data = requests.get(url, params).json()['message']
        if not data['items']:
            break
        population_data.extend(data['items'])
        cursor = data['next-cursor']

    return population_data

@retry(tries=10, delay=1, backoff=2, max_delay=300)
def get_sample(url, query_params, population_size, sample_size):
    params = {'sample': min(100, sample_size)}
    params.update(query_params)

    sample = []
    sampled_dois = set()
    attempts = 5
    while attempts >= 0 and len(sample) < sample_size:
        sample_subset = requests.get(url, params).json()['message']['items']
        new_dois = False
        for s in sample_subset:
            if len(sample) == sample_size:
                break
            if s['DOI'] not in sampled_dois:
                new_dois = True
                sampled_dois.add(s['DOI'])
                sample.append(s)
        if not new_dois:
            attempts -= 1
    return sample

def get_data(context):
    url = f'{API_URL}/{context["api-request"]}'
    query_params = context.get('query-parameters', {})
    if query_params:
        query_params = process_query_parameters(query_params)
    query_params['mailto'] = MAILTO

    # default value for sample size if the key does not exist in the context file
    sample_size = context.get('sample-size', 5)
 
    population_size, extracted = get_population_info(url, query_params)

    sample = []
    # if the population is not large enough, do not return any data points
    if 'min-population-size' in context and population_size < context['min-population-size']:
        pass
    
    # if we already extracted the entire population, return all extracted data points
    elif len(extracted) < 1000:
        if len(extracted) <= sample_size:
            sample = extracted
        else:
            sample = random.sample(extracted, sample_size)

    # if population is smaller than sample size, extract the entire population
    elif population_size <= sample_size:
        sample = get_population(url, query_params)

    # otherwise, sample from the API
    else:
        sample = get_sample(url, query_params, population_size, sample_size)

    return sample

def save_sample(context, sample):
    session = boto3.Session()
    s3 = session.resource('s3')
    if not sample:
        obj = s3.Object(SAMPLES_BUCKET, f'{SAMPLES_PREFIX}/{context["s3-prefix"]}.empty')
        obj.put(Body=json.dumps(context))
    elif OUTPUT_JSONL:
        sample = [json.dumps({'data-point-id': i, 'data-point': s}) for i, s in enumerate(sample)]
        obj = s3.Object(SAMPLES_BUCKET, f'{SAMPLES_PREFIX}/{context["s3-prefix"]}.jsonl')
        obj.put(Body='\n'.join(sample))
    else:
        for i, s in enumerate(sample):
            obj = s3.Object(SAMPLES_BUCKET, f'{SAMPLES_PREFIX}/{context["s3-prefix"]}/{i}/{OUTPUT_FILE}')
            obj.put(Body=json.dumps(s))
    return len(sample)

def get_empty_sample_context(key):
    s3client = boto3.client('s3')
    context = s3client.get_object(Bucket=SAMPLES_BUCKET, Key=key)['Body'].read().decode('utf-8')
    s3client.delete_object(Bucket=SAMPLES_BUCKET, Key=key)
    return context


spark = SparkSession.builder \
            .config('spark.jars.packages', 'org.apache.hadoop:hadoop-aws:2.8.5') \
            .getOrCreate()

# load sample contexts
contexts = spark.sparkContext.textFile(SAMPLES_CONTEXTS, minPartitions=1000)
contexts = contexts.map(json.loads)

# download and save samples
samples = contexts.map(lambda c: (c, get_data(c)))

data_points_count = samples.map(lambda s: save_sample(s[0], s[1]))
data_points_count = data_points_count.sum()

# generate metadata file
session = boto3.Session()
s3 = session.resource('s3')
obj = s3.Object(SAMPLES_BUCKET, f'{SAMPLES_PREFIX}/{METADATA_FILE}')
obj.put(Body=json.dumps({'total-data-points': data_points_count, 'collected-date': TODAY}))

# generate empty samples contexts file
s3client = boto3.client('s3')
paginator = s3client.get_paginator('list_objects_v2')
empty_sample_files = []
for page in paginator.paginate(Bucket=SAMPLES_BUCKET, Prefix=SAMPLES_PREFIX):
    for obj in page['Contents']:
        if obj['Key'].endswith('empty'):
            empty_sample_files.append(obj['Key'])

if empty_sample_files:
    empty_sample_files = spark.sparkContext.parallelize(empty_sample_files, numSlices=len(empty_sample_files))
    empty_samples = empty_sample_files.map(get_empty_sample_context)
    empty_samples.coalesce(1).saveAsTextFile(EMPTY_SAMPLES)

