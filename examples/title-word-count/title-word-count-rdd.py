import json
import sys

from pyspark.sql import SparkSession


CONFIG = json.loads(sys.argv[1])
INPUT = CONFIG["input"]
OUTPUT = CONFIG["output"]


def get_words(data_point):
    item = data_point["data-point"]
    title = item.get("title", [])
    if not title:
        return []
    title = title[0].lower()
    return title.split()


spark = SparkSession.builder.config(
    "spark.jars.packages", "org.apache.hadoop:hadoop-aws:2.8.5"
).getOrCreate()

# read the input files
# the resulting RDD item is a line from the input file (string)
data = spark.sparkContext.textFile(INPUT, minPartitions=1000)

# map <line (string)> -> <JSON object (dict)>
data = data.map(lambda i: json.loads(i))

# extract title words from the JSON object
# flatMap performs two operations:
# 1. map <JSON object (dict)> -> <title words (list of strings)>
#    the resulting RDD item is a list of words
# 2. flatten the lists
#    the resulting RDD item is a single word (string)
words = data.flatMap(get_words)

# map <word (string)> -> <(word, 1) (tuple)>
counts = words.map(lambda w: (w, 1))

# group by word and add all 1s in each group
# the resulting RDD item is a tuple (word, count)
counts = counts.reduceByKey(lambda a, b: a + b)

# filter only words with count > 10
counts = counts.filter(lambda wc: wc[1] > 10)

# sort the RDD by count descending
counts = counts.sortBy(lambda wc: wc[1], ascending=False)

# map <(word, count) (tuple)> -> "word count" (string)
counts = counts.map(lambda wc: f"{wc[0]} {wc[1]}")

# save the result as text files
counts.coalesce(10).saveAsTextFile(OUTPUT)
