import json
import sys

from pyspark.sql import SparkSession
from pyspark.sql.functions import udf, explode
from pyspark.sql.types import ArrayType, StringType


CONFIG = json.loads(sys.argv[1])
INPUT = CONFIG["input"]
OUTPUT = CONFIG["output"]


# user-defined function
@udf(returnType=ArrayType(StringType()))
def get_words(data_point):
    data_point = json.loads(data_point)
    item = data_point["data-point"]
    title = item.get("title", [])
    if not title:
        return []
    title = title[0].lower()
    return title.split()


spark = SparkSession.builder.config(
    "spark.jars.packages", "org.apache.hadoop:hadoop-aws:2.8.5"
).getOrCreate()

# read data as text
# this results in a DataFrame with one column "value"
data = spark.read.text(INPUT)

# apply get_words UDF
# this results in a DataFrame with one column "wordlist"
# a single "wordlist" value is a list of words
words = data.select(get_words("value").alias("wordlist"))

# flatten the lists of words
# this results in a DataFrame with one column "word"
# a single "word"value is a word
words = words.select(explode(words.wordlist).alias("word"))

# group by word and count rows within each group
# this adds a second column "count"
counts = words.groupBy("word").count()

# filter rows with count > 10
counts = counts.filter(counts["count"] > 10)

# sort by count descenting
counts = counts.sort(counts["count"].desc())

# save in CSV format
counts.write.format("csv").save(OUTPUT)
