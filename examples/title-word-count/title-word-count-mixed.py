import json
import sys

from pyspark.sql import SparkSession


CONFIG = json.loads(sys.argv[1])
INPUT = CONFIG["input"]
OUTPUT = CONFIG["output"]


def get_words(data_point):
    item = data_point["data-point"]
    title = item.get("title", [])
    if not title:
        return []
    title = title[0].lower()
    return title.split()


spark = SparkSession.builder.config(
    "spark.jars.packages", "org.apache.hadoop:hadoop-aws:2.8.5"
).getOrCreate()

# read the input files
# the resulting RDD item is a line from the input file (string)
data = spark.sparkContext.textFile(INPUT, minPartitions=1000)

# map <line (string)> -> <JSON object (dict)>
data = data.map(lambda l: json.loads(l))

# extract title words from the JSON object
# flatMap performs two operations:
# 1. map <JSON object (dict)> -> <title words (list of strings)>
#    the resulting RDD item is a list of words
# 2. flatten the lists
#    the resulting RDD item is a single word (string)
words = data.flatMap(get_words)

# map <word (string)> -> <(word,) (tuple)>
# this input is required for createDataFrame
words = words.map(lambda x: (x,))

# create a DataFrame with one column "word"
words = spark.createDataFrame(words, ["word"])

# group by word and count rows within each group
# this adds a second column "count"
counts = words.groupBy("word").count()

# filter rows with count > 10
counts = counts.filter(counts["count"] > 10)

# sort by count descenting
counts = counts.sort(counts["count"].desc())

# save in CSV format
counts.write.format("csv").save(OUTPUT)
