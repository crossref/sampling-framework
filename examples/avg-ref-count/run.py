import argparse
import base64
import boto3
import json
import logging
import os
import sys


def get_secret(secret_name):
    region_name = "us-east-1"
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )
    response = client.get_secret_value(SecretId=secret_name)
    return response.get('SecretString', base64.b64decode(response.get('SecretBinary', '')))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Estimate average number of references per member using samples')
    parser.add_argument('-n', '--name', type=str, default='avg-ref-count')
    parser.add_argument('-c', '--contexts', type=str, required=True)
    parser.add_argument('-b', '--bucket', type=str, required=True)
    parser.add_argument('-p', '--prefix', type=str, required=True)
    parser.add_argument('-a', '--apiurl', type=str, default='https://api.crossref.org')
    parser.add_argument('-l', '--jsonlines', action='store_true')
    parser.add_argument('-m', '--mailto', type=str, required=True)
    parser.add_argument('-o', '--output', type=str, required=True)
    args = parser.parse_args()

    client = boto3.client('emr', region_name='us-east-1')

    response = client.run_job_flow(
        Name=args.name,
        ReleaseLabel='emr-5.36.0',
        Applications=[{
            'Name': 'Spark'
        }],
        Instances={
            'MasterInstanceType': 'm5.xlarge',
            'SlaveInstanceType': 'm5.xlarge',
            'InstanceCount': 5,
            'KeepJobFlowAliveWhenNoSteps': False,
            'TerminationProtected': False
        },
        Steps=[{
            'Name': 'generate-samples',
            'ActionOnFailure': 'TERMINATE_CLUSTER',
            'HadoopJarStep': {
                'Jar': 'command-runner.jar',
                'Args': ['spark-submit',
                         '--deploy-mode',
                         'cluster',
                         f's3://{get_secret("bucket-name-emr-code")}/sampling-framework/generate-samples.py',
                         json.dumps({'contexts': args.contexts,
                                     'samples-bucket': args.bucket,
                                     'samples-prefix': args.prefix,
                                     'output-jsonl': 1 if args.jsonlines else 0,
                                     'api-url': args.apiurl,
                                     'mailto': args.mailto})
                ]
            }
        },{
           'Name': 'avg-ref-count',
            'ActionOnFailure': 'TERMINATE_CLUSTER',
            'HadoopJarStep': {
                'Jar': 'command-runner.jar',
                'Args': ['spark-submit',
                         '--deploy-mode',
                         'cluster',
                         f's3://{get_secret("bucket-name-emr-code")}/sampling-framework/examples/avg-ref-count.py',
                         json.dumps({'samples-bucket': args.bucket,
                                     'file-pattern': f'{args.prefix}/works/member/*.jsonl' if args.jsonlines else f'{args.prefix}/works/member/*/*/data-point.json',
                                     'format-jsonl': 1 if args.jsonlines else 0,
                                     'output': args.output
                                     })
                ]
            }
        }],
        BootstrapActions=[{
            'Name': 'Python packages',
            'ScriptBootstrapAction': {
                'Path': f's3://{get_secret("bucket-name-emr-code")}/sampling-framework/bootstrap.sh'
            }
        }],
        LogUri=get_secret('s3-url-logs'),
        VisibleToAllUsers=True,
        ServiceRole='EMR_DefaultRole',
        JobFlowRole='EMR_EC2_DefaultRole',
    )

    print(json.dumps(response, indent=4, sort_keys=True, default=str))
