# Estimating average number of references per member

This example shows how we can use the sampling framework to estimate the average number of references per member.

Use `run.py` to run the example.
