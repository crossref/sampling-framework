import json
import sys

from pyspark.sql import SparkSession
from pyspark.sql.functions import avg


CONFIG = json.loads(sys.argv[1])

SAMPLES_BUCKET = CONFIG['samples-bucket']
FILE_PATTERN = CONFIG['file-pattern']
OUTPUT = CONFIG['output']
FORMAT_JSONL = CONFIG.get('format-jsonl', '')

spark = SparkSession.builder \
            .config('spark.jars.packages', 'org.apache.hadoop:hadoop-aws:2.8.5') \
            .getOrCreate()

if FORMAT_JSONL:
    data = spark.sparkContext.textFile(f's3://{SAMPLES_BUCKET}/{FILE_PATTERN}', minPartitions=1000)
    data = data.map(lambda r: json.loads(r))
    data = data.map(lambda r: r['data-point'])
else:
    data = spark.sparkContext.wholeTextFiles(f's3://{SAMPLES_BUCKET}/{FILE_PATTERN}', minPartitions=1000)
    data = data.map(lambda r: json.loads(r[1]))

data = data.map(lambda r: (r.get('member', 'unknown'), len(r.get('reference', []))))
data = spark.createDataFrame(data, ['member', 'reference-count'])

ref_count = data.groupBy('member').agg(avg('reference-count').alias('avg-ref-count'))

ref_count.coalesce(1).write.format('csv').save(OUTPUT)
